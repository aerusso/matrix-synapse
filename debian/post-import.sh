#!/bin/sh

debian/repack-vendored.py

dch -v"$1" New upstream release.
git add debian/changelog
debcommit -m "Prepare changelog entry"
